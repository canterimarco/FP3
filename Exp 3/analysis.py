import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sp
import scipy.signal as si
import math
from scipy.special import j0, j1


def read_data(file):
    data = np.genfromtxt(file, skip_header=18 ,delimiter=",",usecols=range(3,5))
    return data[:,0],data[:,-1]

def read_shit(file, a, b):
    data = np.genfromtxt(file, skip_header=3 ,delimiter=",",usecols=range(a,b))
    return data[:,0],data[:,1],data[:,2],data[:,3],data[:,4]

def read_pars(file, a):
    data = np.genfromtxt(file, skip_header= 3+24*(a-1), skip_footer = 120-24*(a-1), delimiter=";",usecols=range(2,4))
    return data[:,0],data[:,1]

def print_params(par, dpar):
	interv = np.array([1,5,6])
	print('& $x_\mathrm{c}$ / \si{\milli \s} & FWHM / \si{\ms} & Height / arb. unit \\\\ \hline')
	for j in range(1,4):
		print('Peak '+str(j), end = ' ')
		for i in interv:
			print('& \\num{'+str('%f' % par[i+(j-1)*7]).rstrip('0')+' \pm '+str('%f' % dpar[i+(j-1)*7]).rstrip('0')+'}', end = ' ')
		print('\\\\ \r')
	print('\\midrule')


# EOM impedence resonance

freq = np.linspace(12.5, 14.5, 21)
freq = np.append(freq, [13.65, 13.75, 13.55])

Vpp = np.array([310, 302, 294, 286, 274, 256, 236, 212, 180, 146, 114, 104, 106, 128, 155, 172, 196, 216, 234, 255, 270, 102, 115, 110])

plt.figure(figsize = (8,5))
plt.errorbar(freq, Vpp, yerr= 3, fmt='.')
plt.xlabel(r'Frequency / MHz', fontsize= 'large')
plt.ylabel(r'$V_{\mathrm{pp}}$ / mV', fontsize = 'large')
#plt.savefig('eom_impedance_match.pdf', format = 'pdf')


# Modulation depth

b, a = si.butter(6, .01, 'low')

volts = np.linspace(20, 10, 6)

height_side = np.array([])
dheight_side = np.array([])

height_middle = np.array([])
dheight_middle = np.array([])

mean_d = 0
dmean_d= 0

for i in range(1,7):
	t, I = read_data('data/TEK000'+str(i)+'.CSV')
	t = 1000 * t # convert s to ms
	I_fil = si.filtfilt(b, a, I, )
	#np.savetxt('fil000'+str(i)+'.csv', np.array([t, I_fil]).transpose(), delimiter = ';')
	t_, p1, p2, p3, ptot = read_shit('data/fits.csv', 8 * (i-1), 5 + 8 * (i-1))
	par, dpar = read_pars('data/params.csv', i)
	hs = (par[6]/dpar[6]**2 + par[20]/dpar[20]**2) / (1/dpar[6]**2 + 1/dpar[20]**2)
	dhs = np.sqrt(1 / (1/dpar[6]**2 + 1 / dpar[20]**2))
	height_side = np.append(height_side, hs)
	dheight_side = np.append(dheight_side, dhs)
	height_middle = np.append(height_middle, par[13])
	dheight_middle = np.append(dheight_middle, dpar[13])
	#distances:
	d1 = np.abs(par[1] - par[8]) # left + central
	dd1= np.sqrt(dpar[1]**2 + dpar[8]**2)
	d2 = np.abs(par[15] - par[8]) # right + central
	dd2= np.sqrt(dpar[15]**2 + dpar[8]**2)
	dm = (d1+d2)/2
	ddm= 0.5 * np.sqrt(dd1**2 + dd2**2)
	print(dm, ddm)
	mean_d += d1/dd1**2 + d2/dd2**2 # not quite mean yet
	dmean_d += 1/dd1**2 + 1/dd2**2 # not yet actual error of mean
	for iter in range(len(par)): # round results (first significant error digit)
		par[iter] = np.round(par[iter], int(1-np.ceil(np.log10(np.absolute(dpar[iter])))))
		dpar[iter] = np.round(dpar[iter], int(1-np.ceil(np.log10(np.absolute(dpar[iter])))))
	#print(par, dpar)
	#print('\\midrule')
	#print('$V_\\mathrm{pp} = \SI[scientific-notation=false]{' + str(int(volts[i-1]))+'}{\\volt}$', end = ' ')
	#print_params(par, dpar)
	'''plt.figure(figsize = (8, 6))
	plt.plot(t_, p1, 'b', t_, p2, 'g', t_, p3, 'c', t_, ptot, 'r')
	plt.plot(t, I_fil, 'k')
	plt.xlabel(r'$t$ / ms', fontsize = 14)
	plt.ylabel(r'$I$ / arb. unit', fontsize = 14)
	plt.xticks(fontsize = 14)
	plt.yticks(fontsize = 14)
	plt.tight_layout()'''
	#plt.savefig('tek'+str(i)+'.pdf', format = 'pdf')

# ratio of P_c / P_s
prop = height_middle / height_side
dprop= np.sqrt((dheight_middle/height_side)**2 + (height_middle * dheight_side / (height_side**2))**2)
print(prop, dprop)

def bessel0(x,a,b):
	return a * j0(b * x)**2

def bessel1(x, a, b):
	return a * j1(b * x)**2

parC, covC = sp.curve_fit(bessel0, volts[0:5], height_middle[0:5], p0=[1, 0.1], sigma = dheight_middle[0:5], bounds = (0, np.inf))
dparC = np.sqrt(np.diag(covC))
print('Bessel0: ', parC, dparC)

parS, covS = sp.curve_fit(bessel1, volts[0:5], height_side[0:5], p0=[0.15, 0.06264002], bounds = (0, 0.2)) # sigma = dheight_side[0:5],
dparS = np.sqrt(np.diag(covS))
print('Bessel1: ', parS, dparS)

y = np.linspace(10,20, 100)

plt.figure(figsize = (8,5))
plt.errorbar(volts, height_middle, yerr=dheight_middle, fmt= 'r.', label=r'$I_\mathrm{C}$')
plt.errorbar(volts, height_side, yerr=dheight_side, fmt= 'b.', label=r'$I_\mathrm{S}$')
plt.plot(y, bessel0(y, *parC), color = 'r')
plt.plot(y, bessel1(y, *parS), color = 'b')
plt.ylim(0)
plt.legend()
plt.xlabel(r'$V_\mathrm{pp}$ / V', fontsize = 13)
plt.ylabel(r'$I$ / arb. unit', fontsize = 13)
plt.xticks(fontsize = 12)
plt.yticks(fontsize = 12)
plt.tight_layout()
#plt.savefig('bessel_singles.pdf', format = 'pdf')


# Bessel functions
fact = 2
ratios = j0(volts/fact)**2/j1(volts/fact)**2
print(ratios)

# fit Bessel functions
def bessel_fct(x, b):
	return j0(b*x)**2/j1(b*x)**2

popt, pcov = sp.curve_fit(bessel_fct, volts[0:5], prop[0:5], p0 = [ 0.06])#, sigma = dprop[0:5])
dpopt = np.sqrt(np.diag(pcov))
print('ratios: ', popt, dpopt)
# plot P_c / P_s
plt.figure(figsize = (8,5))
plt.errorbar(volts, prop, yerr = dprop, fmt= '.')
plt.plot(y, bessel_fct(y, *popt), color = 'r')
plt.xlabel(r'$V_\mathrm{pp}$ / V', fontsize = 13)
plt.ylabel(r"$I_\mathrm{C}$ / $I_\mathrm{S}}$", fontsize = 13)
plt.ylim(0,10)
plt.xticks(fontsize = 12)
plt.yticks(fontsize = 12)
plt.tight_layout()
#plt.savefig('bessel_ratios.pdf', format = 'pdf')


# calculate the conversion coefficient from ms to MHz
mean_d = mean_d / dmean_d
dmean_d= 1 / np.sqrt(dmean_d)
print(mean_d, dmean_d)

conversion = 13.55 / mean_d # conversion factor, takes seconds gives
dconversion= 13.55 * dmean_d / mean_d**2
print(conversion, dconversion)

plt.show()

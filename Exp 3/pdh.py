import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['font.size'] = 16


from scipy.optimize import curve_fit
import scipy.signal as si
import scipy.constants as sc

from lmfit import minimize, Parameters

def read_data(file):
	data = np.genfromtxt(file, delimiter=",", skip_header = 18, usecols=range(3,5))
	return data[:,0],data[:,-1]

def F(omega,r,x0):
	#fsr = sc.c/(0.3)
	fsr = 934.6043529710606e6
	return (r*(np.exp(1j*(omega-x0)/fsr)-1))/(1-r**2*np.exp(1j*(omega-x0)/fsr))

def B(omega,Omega,r,x0):
	return F(omega,r,x0)*np.conj(F(omega+Omega,r,x0)) - np.conj(F(omega,r,x0))*F(omega-Omega,r,x0)

def pdhsignal(omega,off,A,Omega,phase,r,x0):
	return off + A*(np.real(B(omega,Omega,r,x0))*np.sin(np.deg2rad(phase)) + np.imag(B(omega,Omega,r,x0))*np.cos(np.deg2rad(phase)))

def fit(t,off,A,phi,x0,r,Omega):

	return pdhsignal(t,off,A,Omega,phi,r,x0)

#PDH signal
Omega = 13.55e6
t2f = 934.6043529710606e6/0.00860000000016435

par = np.array(["V_{off}=","A=","\phi=","\omega_0=","r=","\Omega="])


#print(phase)
for i in range(5):
	t,v = read_data("data/F000"+str(i)+"CH3.CSV")
	t = t2f*t
	b, a = si.butter(6, .01, 'low')
	v_filter = si.filtfilt(b, a, v)

	plt.figure()
	plt.plot(t,v)
	plt.plot(t,v_filter)
	popt, pcov = curve_fit(fit, t, v_filter,p0 = [0,0.04,0,0,0.99,13.55e6],method = "dogbox")
	plt.plot(t, fit(t, *popt), 'r')
	print(360+popt[2])
	perr = np.sqrt(np.diag(pcov))
	print(perr)
	for j in range(len(popt)):
		print("$",par[j],popt[j],"\pm",perr[j],"$", end=',')
	plt.xlabel("Frequency / Hz")
	plt.ylabel("Voltage / V")
	print("")

t,v = read_data("data/F0005CH3.CSV")
t = t2f*t
plt.figure()
plt.plot(t,v)
v_filter = si.filtfilt(b, a, v)
plt.plot(t,v_filter)
popt, pcov = curve_fit(fit, t, v_filter,method = "dogbox",bounds= ([-1,0.03,180,2e6,0.993,13.15e6 ],[1,0.1,260,11e6,0.999,13.70e6]  ))
plt.plot(t, fit(t, *popt), 'r')

perr = np.sqrt(np.diag(pcov))
for j in range(len(popt)):
	print("$",par[j],popt[j],"\pm",perr[j],"$", end=',')
print("")
plt.xlabel("Frequency / Hz")
plt.ylabel("Voltage / V")
print(popt[2])

#t = np.linspace(0,1e10,1000000)
#plt.plot(t,fit(t,0,0.04,180,0,0.99,13.55e6))

t,v = read_data("data/F0006CH3.CSV")
t = t2f*t
plt.figure()
plt.plot(t,v)
v_filter = si.filtfilt(b, a, v)
plt.plot(t,v_filter)
popt, pcov = curve_fit(fit, t, v_filter,method = "dogbox",bounds= ([-1,0.01,210,2e6,0.993,12.50e6],[1,0.09,240,11e6,0.999,13.70e6]  ))
plt.plot(t, fit(t, *popt), 'r')

perr = np.sqrt(np.diag(pcov))
for j in range(len(popt)):
	print("$",par[j],popt[j],"\pm",perr[j],"$", end=',')
print("")
print(popt[2])
plt.xlabel("Frequency / Hz")
plt.ylabel("Voltage / V")
#t = np.linspace(-1e8,1e8,10000)
#for i in range(220,240,2):
	#plt.plot(t,fit(t,0,0.04,i,4e6,0.996,13.55e6),label=str(i))
#plt.legend()

plt.show()

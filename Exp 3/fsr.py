import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sp

def lorentzian(x, x0, a, g, b):
	return a * (g**2) / ((x-x0)**2 + (g**2)) + b

def read_data(file):
	data = np.genfromtxt(file, skip_header=18 ,delimiter=",",usecols=range(3,5))
	return data[:,0],data[:,1]

t, I = read_data('data/TEK0007.CSV')
t = t * 1000 # convert to ms
p_left, covleft = sp.curve_fit(lorentzian, t[(t <1) & (t > 0)], I[(t <1) & (t > 0)], p0 = [0.6, 0.13, 0.3, 0.01])
dp_left= np.sqrt(np.diag(covleft))

p_right, covright = sp.curve_fit(lorentzian, t[(t <9.6) & (t > 8.6)], I[(t <9.6) & (t > 8.6)], p0 = [9.26, 0.13, 0.3, 0.01])
dp_right= np.sqrt(np.diag(covright))

print(p_left[0], dp_left[0])
print(p_right[0], dp_right[0])

fsr_t = np.abs(p_right[0] - p_left[0])
dfsr_t = np.sqrt(dp_right[0]**2 + dp_left[0]**2)
print(fsr_t, dfsr_t)

nu_fsr = 108.674924762 * fsr_t
dnu_fsr= np.sqrt((0.134735541375*fsr_t)**2 + (108.674924762 * dfsr_t)**2)
print(nu_fsr, dnu_fsr)

L = 0.5e-6 * 299792458 / nu_fsr
dL= 0.5e-6 * 299792458 * dnu_fsr / nu_fsr**2
L = np.round(L, int(1-np.ceil(np.log10(np.absolute(dL)))))
dL = np.round(dL, int(1-np.ceil(np.log10(np.absolute(dL)))))
print(L, dL)

plt.figure(figsize = (8,5))
plt.plot(t, I)
plt.plot(t[(t <1) & (t > 0)], lorentzian(t[(t <1) & (t > 0)], *p_left))
plt.plot(t[(t <9.6) & (t > 8.6)], lorentzian(t[(t <9.6) & (t > 8.6)], *p_right))
plt.xlabel(r'$t$ / ms', fontsize = 13)
plt.ylabel(r'$I$ / arb. unit', fontsize = 13)
plt.xticks(fontsize = 12)
plt.yticks(fontsize = 12)
plt.tight_layout()
#plt.savefig('fsr.pdf', format = 'pdf')
plt.show()

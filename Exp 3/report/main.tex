\documentclass[a4paper,10pt]{article}

\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}

\usepackage{textcomp}           % Extra Symbole (Grad Celsius etc.)
\usepackage{amssymb,amsmath}    % Schöne Formeln (AMS = American Mathematical Society)
\usepackage{graphicx}           % Bilder und Seitenränder
\usepackage{subcaption}			% captions for subfigures
\usepackage{booktabs}           % Schönere Tabellen
\usepackage{colortbl}           % Farbige Tabellen
\usepackage{gensymb}
%\usepackage{tcolorbox}			% schöne bunte Boxen
\usepackage{mathtools}			% \mathclap für ordentliche \underbrace-			environments
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}			% Pagelayout mit \newgeometry, \restoregeometry
\usepackage{float}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{float}
\usepackage{braket}
\usepackage{caption}
\usepackage[per-mode=reciprocal,output-decimal-marker={.},binary-units=true,separate-uncertainty=false, scientific-notation=false]{siunitx}
\usepackage[breaklinks=true,colorlinks=true,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage{physics}
\usepackage{url}
\usepackage{subcaption}
%\usepackage{calrsfs}
\DeclareMathAlphabet{\pazocal}{OMS}{zplm}{m}{n}
\usepackage{tikz}
\usetikzlibrary{decorations, positioning, intersections, calc, shapes,arrows, scopes}
\usepackage{pgfplots}
\usepackage{bodegraph}
\usepackage{circuitikz}
\usepackage{chemfig}
\usepackage{chemformula}
\usepackage[toc,page]{appendix}
\usepackage{verbatim}

\DeclareSIUnit\elementarycharge{e}

\newcommand{\dif}{\mathrm{d}}

\bibliographystyle{unsrtnat}

\renewcommand{\k}{\mathbf{k}}
\begin{document}
\begin{titlepage}
 \begin{center}
	\Large{Advanced laboratory course 3}
	\end{center}
	\begin{center}
	 \LARGE{\textbf{Pound-Drever-Hall Laser Stabilization}}
	\end{center}

	\begin{center}

	\large Marco \textsc{Canteri} \\
	marco.canteri@student.uibk.ac.at\\
	\large Maximilian \textsc{Münst} \\
	maximilian.muenst@student.uibk.ac.at
	\end{center}

	\begin{center}
	\vspace{1cm}
	Innsbruck, \today
	\vspace{1cm}
	\end{center}

	\begin{abstract}
		The aim of the experiment was to analyze the Pound-Drever-Hall technique for laser frequency stabilization. First we studied the carrier and sidebands signals created by the EOM, then we analyzed the proper Pound-Drever-Hall error signal, which can be used as feedback for the laser.
  \end{abstract}
    \vspace{1cm}

	\begin{center}
	\includegraphics[scale=0.56]{img/uibk.PNG}
	\end{center}

\end{titlepage}


\section{Introduction}
The Pound-Drever-Hall laser locking technique is a widely popular state of the art method to stabilize the frequency of a laser. The idea is to lock the laser frequency to the output of a Fabry-Perot cavity. One can achieve stability comparable to frequency stability of pulsars \cite{script}. The technique can be applied in experiments that require very precise measurements, for example gravitational wave detection.

\section{Theoretical Background}
\subsection{Pound Drever Hall technique}
The Pound-Drever-Hall technique uses a Fabry-Perot cavity to measure the frequency and feed it back into the laser to achieve stabilization. Ideally if we use a cavity, the transmitted light shows several resonances, spaced by the free spectral range (FSR). We want to operate on one resonance, but the frequency fluctuations of the laser will inevitably bring the signal out of resonance. This can be adjusted with a feedback signal. Unfortunately, we cannot directly use the transmitted or the reflected signal, because it is symmetric with respect to the resonance, which means that the feedback cannot distinguish between the two sides of the resonance and hence the direction of the correction. A simple idea is to use the derivative of the reflected signal, which displays an asymmetry with respect to the resonance, and use it as a feedback signal. This can be done with the setup shown in figure \ref{fig_setup}. An electro-optic modulator (EOM) is used to vary the frequency sinusoidally, the reflected signal produced is compared to the oscillation of the EOM in order to determine the side of the resonance we are on. Now, we are going to describe this model mathematically. This derivation is based on the paper \cite{script}, where the technique is explained in more detail, here we summarize the most important parts.\\
We are always going to refer to the fig. \ref{fig_setup}, and we will describe the beam mathematically following the path from the laser to the Pound-Drever-Hall signal. The laser beam can be described as $E_i = E_0 e^{i\omega t}$, where $\omega$ is the laser frequency. The EOM modulation acts like a phase modulator on the field, the signal after the EOM is
\[E_i = E_0e^{i(\omega t + \beta\sin(\Omega t))},\]
where $\beta$ is the modulation index, and $\Omega$ is the modulation frequency. We can expand the exponential using the Bessel functions $J_n$, as follows
\[E_i = E_0\left[\sum_{n=-\infty}^\infty J_n(\beta)e^{i(\omega + n\Omega)t)}\right]. \]
Keeping only the terms $n=\pm 1$, and using the relation $J_{-n}(z) = (-1)^nJ_n(z)$, we can approximate the beam as
\[E_i \approx E_0\left[J_0(\beta)e^{i\omega t} +J_1(\beta)e^{i(\omega + \Omega)t)} - J_1(\beta)e^{i(\omega -\Omega)t)}\right].\]
From this expression we can identify three signals with three different frequencies: the carrier at frequency $\omega$ and two sidebands with frequencies $\omega \pm \Omega$.\\
After passing the EOM, the beam reaches the cavity, where the reflected signal can be described with a function $F(\omega)$, which is defined as the ratio between the reflected light of the cavity and the incident light in the cavity. For a Fabry-Perot cavity with a FSR of $\Delta \nu_\mathrm{fsr}$, and mirrors with reflectivity $r$, $F(\omega)$ has the following form
\[ F(\omega) = \frac{r^2\left( e^{i\omega/\Delta \nu_\mathrm{fsr}}-1\right)}{1-r^2e^{i\omega/\Delta \nu_\mathrm{fsr}}}.\]
To obtain the reflected beam, we have to multiply the incident beam by this function, hence we obtain
\[E_r = E_0\left[J_0(\beta)F(\omega)e^{i\omega t} +J_1(\beta)F(\omega +\Omega)e^{i(\omega + \Omega)t)} - J_1(\beta)F(\omega-\Omega)e^{i(\omega -\Omega)t)}\right].\]
After the cavity we measure the reflected beam with a photodiode. Therefore we  need to square the field to get the measured power
\begin{multline}\label{measuredpower}
\qquad\qquad \qquad\qquad P_r = |E_r|^2 = P_c|F(\omega)|^2 + P_s\left\{|F(\omega +\Omega)|^2 + |F(\omega -\Omega)|^2\right\}  \\
 + 2\sqrt{P_sP_c} \left\{\text{Re}\left[F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega)\right]\cos(\Omega t) + \text{Im}\left[F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega)\right]\sin(\Omega t)\right\},
 \end{multline}
where we left out the term oscillating at $2\Omega$. Moreover we defined the power of the carrier $P_\mathrm{C} = J_0^2(\beta)|E_0|^2$, and the power of the sideband $P_\mathrm{S} =J_1^2(\beta)|E_0|^2$. This last equation shows that in the signal measurement there is a wave of frequency $\omega$, but with a beat patter at frequency $\omega\pm \Omega$. However more interesting is the last term, which corresponds to the interference between the carrier and the sidebands. This term oscillates at frequency $\Omega$, and therefore contains the position of the resonance.\\
The measured signal is mixed with the signal from the local oscillator used to drive the EOM, but with a different phase $\phi$. If we consider only the last term in  equation \eqref{measuredpower} and we multiply by $\sin(\Omega t +\phi)$ and apply trigonometric identities, it yields the following form for the final Pound-Drever-Hall signal
\begin{equation}\label{pdhsignal}
\epsilon = 2\sqrt{P_sP_c} \left\{\text{Re}\left[F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega)\right]\sin(\phi) + \text{Im}\left[F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega)\right]\cos(\phi)\right\}.
\end{equation}
where only the DC terms are considered. This is the most general form of the Pound-Drever-Hall signal, but there are two interesting limits. If $\Omega \ll \Delta \nu_\mathrm{fsr}/\mathcal{F}$, where $\mathcal{F}$ is the finesse of the cavity, we are in the slow modulation limit, i.e. the frequency is dithered slowly by the EOM. This allows the standing wave inside the cavity to respond which can be seen in the reflected beam. In this case, it can be shown that the quantity $F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega)$ is completely real and hence in expression \eqref{pdhsignal} only the first term survives, if the phase $\phi$ is set to $\pi/2$. In the opposite case, fast modulation $\Omega \gg \Delta \nu_\mathrm{fsr}/\mathcal{F}$, the quantity $F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega)$ is completely imaginary, and thus in equation \eqref{pdhsignal} the second part survives, with $\phi$ set to 0. In this case the change of modulation is much faster than the response of the cavity, but the cavity is not a limitation, rather it provides an average signal stored inside the cavity. The error signal $\epsilon$ is due to the interference between the carrier given by the averaged cavity wave, and the sidebands which carry information about the fluctuation in the incident beam. Therefore, the Pound-Drever-Hall will respond immediately even in case of fast modulation.
This is also the reason why the reflected beam is used, instead of the transmitted one. If we used the transmitted beam, in the fast modulation regime the signal would react slower to the fluctuation of the frequency, therefore the bandwidth of the cavity plays a role, while it does not affect the reflected beam.

\subsection{Gaussian beam}
The beam emitted from the laser is a Gaussian beam, which is a wave with wavenumber $k$, solution of the Helmoltz equation with complex amplitude given by \cite{saleh}
\[U(r) = A_0 \frac{W_0}{W(z)}\exp\left\{-\frac{\rho^2}{W^2(z)}\right\}\exp\left\{-ikz-ik\frac{\rho^2}{2R(z)}+i\arctan(z/z_0)\right\},\]
where $A_0$ and $z_0$ are constant parameters, respectively an amplitude and the Rayleigh range, $z$ is the direction of propagation, while $\rho = x^2+y^2$. $W(z)$ and $R(z)$ are two relevant physical quantities, i.e. the beam width and the wavefront radius of curvature.
The beam width can be expressed as
\[W(z) = W_0 \sqrt{1-\left(\frac{z}{z_0}\right)^2} \qquad W_0 = \sqrt{\frac{\lambda z_0}{\pi}},\] therefore the beam at $z=0$ has the minimum width $W_0$, which is where the waist of the beam is located.\\
If we look at the intensity of the beam, we have
\[I = |U|^2 = I_0 \left(\frac{W_0}{W(z)}\right)\exp\left\{-\frac{2\rho^2}{W^2(z)}\right\}.\]
 Clearly, for a fixed position $z$ we have the shape of a Gaussian function in the x-y plane, where the width of the Gaussian function is indeed given by $W(z)$. The behaviour of $W(z)$ is also interesting, as we can see from fig. \ref{beamradius}. It assumes a minimum in $z=0$, and diverges from $z\to \pm \infty$. For large $z$ the increase is linear. Furthermore it can be shown that inside the circle of radius $W(z)$ there is 86\% of the power for a wavefront at $z$.\\
A Gaussian beam is also a mode of a resonator with spherical mirrors, satisfying the boundary condition at the mirrors' surface. For a Gaussian beam to interfere constructively inside the resonator, the curvature of the wavefront must coincide with the curvature of the mirror $R_m$. If the two mirrors are separated by a distance $d$, this condition turns into a condition for the waist radius $W_0$. If we place one mirror at $-d/2$ and the other at $d/2$ and the waist coincides with $z=0$, then we have that \cite{saleh}
\[z_0 = \frac{d}{2}\sqrt{2\frac{|R_m|}{d} -1}\qquad W_0 = \sqrt{ \frac{d\lambda}{2\pi}\sqrt{2\frac{|R_m|}{d} -1}}.\]
In the case of symmetrical confocal mirrors, we have $d/|R_m| = 1$, which implies that the waist diameter inside the resonator is
\begin{equation}\label{bestwaist}
2W_0 = 2\sqrt{\frac{\lambda d}{2\pi}}
\end{equation}
\begin{figure}
\centering
\includegraphics[width = 0.8\textwidth]{img/beamtest}
\caption{Gaussian beam inside a spherical resonator. The vertical axis show the width of the beam $W(z)$, where the center of the cavity coincides with $z=0$. Image from \cite{saleh}.}\label{beamradius}
\end{figure}


\section{Experimental Setup}
The practical challenge in this experiment was to arrange all elements and properly align the setup, which is sketched in fig. \ref{fig_setup}. A Helium-Neon laser is used to generate the laser beam we are working with. The beam first passes through a Faraday isolator, which prevents any reflections that occur in the residual setup to reenter the laser's cavity and cause instabilities in the frequency or intensity of the beam.
\\
The output of the isolator is filtered by a polarizer and redirected by means of two mirrors. We used both mirrors to adjust the beam's height in order to allow the beam to pass through the electro-optical modulator. The EOM is used to modulate sidebands onto the beam. After the EOM a lens with a focal length of $f= \SI{100}{\milli \m}$ was positioned in order to make the beam divergent. Then the direction of the beam is turned again by two more mirrors. A second lens ($f = \SI{200}{\milli \m}$) is then used to refocus the diverging beam.
\\
Using a waist meter we measured the width of the beam along its way and tuned the position of the second lens until we found a configuration where the waist of the beam after the second lens was \SI{250}{\micro \m}, which, according to \eqref{bestwaist}, is the waist of a Gaussian beam inside a resonator of \SI{15}{\cm}. Then a Fabry-Perot etalon of the calculated length was constructed such that the waist of the beam coincided with the center of the cavity. We first set up the rearmost mirror of the etalon, orienting the beam back into the direction it was coming from and only then we set up the entrance mirror of the Fabry-Perot cavity.
\\
Finally, a beam splitter was placed in the path of the beam between the etalon and the second lens in order to examine the reflected intensity of the cavity by using a photodiode. At the rear end of the Fabry-Perot interferometer a second photodiode was placed.
\\
In order to see the Pound Drever Hall effect, one has to mix the signal that is fed into the EOM with the signal that is measured with the photodiode after the beam splitter. The mixer essentially multiplies both input signals, the output is first passed through a low-pass filter and then analyzed using an oscilloscope.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{img/setup}
	\caption{Sketch of the used setup. The red lines represent the laser path, black is for electrical signal paths. }
	\label{fig_setup}
\end{figure}
%In sec. \ref{sec_matching}, we put the splitter in between the function generator and the EOM, as is the case in the main experiment. However, instead of feeding the signal into the mixer, we connected the respective port of the splitter to an oscilloscope, which was used to measure the peak to peak voltage that occurred in the EOM while varying the frequency of the oscillator in order to find the frequency at which the impedance is minimized.

\section{Analysis}
\subsection{Matching the impedance of the electro-optical modulator}
\label{sec_matching}
Before starting the measurement on the laser itself, we had to find the best driving frequency setting for the EOM, i.e. we have to do impedance matching of the EOM with the function generator in order to transmit as much power as possible. In order to find the correct setting the input signal was introduced into a splitter leading to the EOM and an oscilloscope. The peak-to-peak voltage of the signal extracted with the oscilloscope had to be minimized by tuning the frequency in order to minimize the power losses. The result of the conducted measurement is shown in fig. \ref{fig_freq_matching}.
\\
As one can easily spot, the minimum is located at \SI{13.65}{\mega \hertz}. Hence, this is the modulation frequency we wanted to use to conduct the measurements.
\begin{figure}[H]
	\centering
	\includegraphics[width = 0.8 \textwidth]{img/eom_impedance_match.pdf}
	\caption{Plot of the measured peak-to-peak voltages depending on the frequency. The impedance minimum is encountered at \SI{13.65}{\mega \hertz}. The error bars are estimated by the fluctuations of the value on the oscilloscope.}
	\label{fig_freq_matching}
\end{figure}

\subsection{Variation of the modulation depth}
The data used in this part and in the next part of the analysis was not recorded by us but was given to us in order to properly conduct the analysis. The reason for that is that unexpected problems with the He-Ne leaser occurred in the form of a high frequency noise in the signal of the photodiode. One can assume that the laser was the problem as switching it off and on again resolved the problem for about $10$-$20$ seconds, then the noise returned.
\\
On the data set handed to us by the supervisor a modulation frequency of \SI{13.55}{\mega \hertz} was used, which is only slightly different form what we measured earlier. First one can take a look at the influence of the modulation voltage on the measured signal.
The amplitude of the oscillator driving the EOM was changed from \num{10} to \SI{20}{\volt} peak-to-peak voltage in steps of \SI{2}{\volt}. We applied a Butterworth low-pass filter in order to decrease the noise in the data, then a sum of three Voigt curves was fitted to the smoothed data. The result is depicted in fig. \ref{fig_volt_plot}. The values for the center position, the FWHM and the height of the peaks are listed in tab. \ref{tab_volt_params}.
\\
As one can see in fig. \ref{fig_volt_plot}, the height of the sideband peaks decreases with decreasing modulation voltage, apart from the plot at \SI{10}{\volt}. We are not quite sure why this is happening, as the bulge in the data that is caused by the sidebands is getting visibly smaller also in the data recorded at \SI{10}{\volt}. Furthermore, one can see that the lower frequency sideband has a visibly greater height than the higher sideband, which is not the case with the other fits. Changing the starting parameters of the fit did not change the outcome, but one can see in the following sections of the report, that the values derived from the data set at \SI{10}{\volt} seem to be quite off.
\\
Furthermore, tab. \ref{tab_volt_params} indicates that the positions of the sideband peaks are not at a constant distance from the position of the carrier peak, even though that would ideally be the case. The highest average distance we measured is \SI{0.1388(4)}{\ms} at \SI{18}{\volt}, the lowest is at \SI{10}{\volt} with \SI{0.09(1)}{\ms}. One can only speculate as to why these variations are observed. Presumably they are due to faulty elements in the setup, maybe the EOM. However, as the second lowest distance is already \SI{0.114(1)}{\ms} we have one further reason to question the reliability of the parameters we get from data measured at \SI{10}{\volt}.
\\
In the next step one can plot the heights of the carrier and sidebands as a function of the respective voltage that was used to record the data. This can be seen in fig. \ref{fig_heights}. Also, we fitted a squared zeroth and first order Bessel function to the recorded heights of the carrier and the sideband respectively:
\begin{equation}
		I_\mathrm{C} = a_\mathrm{C} J_0^2(b_\mathrm{C} V) \qquad	I_\mathrm{S} = a_\mathrm{S} J_1^2(b_\mathrm{S} V),
\end{equation}
where $a_\mathrm{C},a_\mathrm{S},b_\mathrm{C},b_\mathrm{S}$ are parameters of the fit, which we expect to be the same for the carrier and sidebands.  However, the fit could only be done by ignoring the respective value at \SI{10}{\volt}, as it is way off the line indicated by the other values. The remaining points however are reasonably close to the fitted functions.
Additionally, the parameters that were extracted from the fits independently are in agreement within their respective error margin. The respective parameters for the carrier height was $a_\mathrm{C} = \num{0.2 \pm 0.06}$, $b_\mathrm{C} = \SI{0.063 \pm 0.002}{\per \volt}$ and for the sideband intensity we measured $a_\mathrm{S} = \num{0.2 \pm 0.005}$ and $b_\mathrm{S} = \SI{0.05 \pm 0.02}{\per \volt}$. This seems to justify disregarding the value at \SI{10}{\volt} as one reaches a consistent result.
\\
As a follow up, we also tried to fit the ratio of the heights to the ratio of the Bessel functions:
\begin{equation}
	\label{eq_ratio}
	\frac{I_\mathrm{C}}{I_\mathrm{S}} = \frac{J_0(b V)}{J_1(b V)}
\end{equation}
Here, $I_\mathrm{C}$ and $I_\mathrm{S}$ denote the carrier and sideband peak heights, $b$ is the fit parameter. From the fit we get $b = \SI{0.056 \pm 0.001}{\per \volt}$, which overlaps with $b_\mathrm{S}$ and is reasonably close to the previously determined $b_\mathrm{C}$, as expected. The plot of the data plus the fit is depicted in fig. \ref{fig_ratios}. As was the case in the previous part, the value measured at \SI{10}{\volt} seems to be clearly off, which is why we chose to ignore it in the fit.
\begin{table}[H]
	\centering
	\caption{List of the center position, FWHM and height of all fits conducted in fig. \ref{fig_volt_plot}. Peak 1 describes the lower frequency sideband, peak 2 the carrier signal and peak 3 the higher frequency sideband at the respective voltage. }
	\begin{tabular}{c|c|c|c}
		\midrule
		$V_\mathrm{pp} = \SI[scientific-notation=false]{20}{\volt}$ & $x_\mathrm{c}$ / \si{\milli \s} & FWHM / \si{\ms} & Height / arb. unit \\ \hline
		Peak 1 & \num{-0.1403 \pm 0.0003} & \num{0.1652 \pm 0.0008} & \num{0.05 \pm 0.02} \\
		Peak 2 & \num{-0.0233 \pm 0.0001} & \num{0.0908 \pm 0.0003} & \num{0.083 \pm 0.0003} \\
		Peak 3 & \num{0.0948 \pm 0.0003} & \num{0.156 \pm 0.0009} & \num{0.0491 \pm 0.0001} \\
		\midrule
		\midrule
		$V_\mathrm{pp} = \SI[scientific-notation=false]{18}{\volt}$ & $x_\mathrm{c}$ / \si{\milli \s} & FWHM / \si{\ms} & Height / arb. unit \\ \hline
		Peak 1 & \num{-0.1676 \pm 0.0006} & \num{0.157 \pm 0.004} & \num{0.03 \pm 0.01} \\
		Peak 2 & \num{-0.02851 \pm 0.00006} & \num{0.1203 \pm 0.0008} & \num{0.1064 \pm 0.0005} \\
		Peak 3 & \num{0.1101 \pm 0.0005} & \num{0.137 \pm 0.001} & \num{0.0311 \pm 0.0005} \\
		\midrule
		\midrule
		$V_\mathrm{pp} = \SI[scientific-notation=false]{16}{\volt}$ & $x_\mathrm{c}$ / \si{\milli \s} & FWHM / \si{\ms} & Height / arb. unit \\ \hline
		Peak 1 & \num{-0.1608 \pm 0.0004} & \num{0.146 \pm 0.001} & \num{0.033 \pm 0.002} \\
		Peak 2 & \num{-0.03456 \pm 0.00003} & \num{0.1107 \pm 0.0005} & \num{0.1204 \pm 0.0004} \\
		Peak 3 & \num{0.093 \pm 0.0004} & \num{0.1328 \pm 0.0008} & \num{0.0299 \pm 0.0004} \\
		\midrule
		\midrule
		$V_\mathrm{pp} = \SI[scientific-notation=false]{14}{\volt}$ & $x_\mathrm{c}$ / \si{\milli \s} & FWHM / \si{\ms} & Height / arb. unit \\ \hline
		Peak 1 & \num{-0.1603 \pm 0.0009} & \num{0.154 \pm 0.002} & \num{0.027 \pm 0.003} \\
		Peak 2 & \num{-0.03828 \pm 0.00005} & \num{0.1087 \pm 0.0008} & \num{0.1303 \pm 0.0008} \\
		Peak 3 & \num{0.0874 \pm 0.0008} & \num{0.134 \pm 0.002} & \num{0.0256 \pm 0.0008} \\
		\midrule
		\midrule
		$V_\mathrm{pp} = \SI[scientific-notation=false]{12}{\volt}$ & $x_\mathrm{c}$ / \si{\milli \s} & FWHM / \si{\ms} & Height / arb. unit \\ \hline
		Peak 1 & \num{-0.149 \pm 0.002} & \num{0.123 \pm 0.004} & \num{0.019 \pm 0.002} \\
		Peak 2 & \num{-0.0364 \pm 0.0001} & \num{0.102 \pm 0.001} & \num{0.146 \pm 0.001} \\
		Peak 3 & \num{0.079 \pm 0.002} & \num{0.113 \pm 0.004} & \num{0.016 \pm 0.002} \\
		\midrule
		\midrule
		$V_\mathrm{pp} = \SI[scientific-notation=false]{10}{\volt}$ & $x_\mathrm{c}$ / \si{\milli \s} & FWHM / \si{\ms} & Height / arb. unit \\ \hline
		Peak 1 & \num{-0.12 \pm 0.02} & \num{0.17 \pm 0.01} & \num{0.05 \pm 0.02} \\
		Peak 2 & \num{-0.0431 \pm 0.0006} & \num{0.088 \pm 0.01} & \num{0.13 \pm 0.03} \\
		Peak 3 & \num{0.05 \pm 0.01} & \num{0.143 \pm 0.007} & \num{0.035 \pm 0.008} \\
		\midrule
	\end{tabular}
	\label{tab_volt_params}
\end{table}
\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.45 \textwidth}
    \centering
    \includegraphics[height=6cm]{img/tek1.pdf}
    \caption{\SI{20}{\volt}}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45 \textwidth}
    \centering
    \includegraphics[height=6cm]{img/tek2.pdf}
    \caption{\SI{18}{\volt}}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45 \textwidth}
    \centering
    \includegraphics[height=6cm]{img/tek3.pdf}
    \caption{\SI{16}{\volt}}
  \end{subfigure}
  ~
  \begin{subfigure}[t]{0.45 \textwidth}
    \centering
    \includegraphics[height=6cm]{img/tek4.pdf}
    \caption{\SI{14}{\volt}}
  \end{subfigure}
	~
  \begin{subfigure}[t]{0.45 \textwidth}
    \centering
    \includegraphics[height=6cm]{img/tek5.pdf}
    \caption{\SI{12}{\volt}}
  \end{subfigure}
	~
  \begin{subfigure}[t]{0.45 \textwidth}
    \centering
    \includegraphics[height=6cm]{img/tek6.pdf}
    \caption{\SI{10}{\volt}}
  \end{subfigure}
  \caption{Depiction of the filtered data and conducted fits. All fit functions follow the Voigt profile. The left sideband peak is colored in blue, the carrier peak is green and the right sideband peak is plotted in cyan. The sum of the three peaks is shown as a red line. The FWHM, the height and the center of the peaks are listed in tab. \ref{tab_volt_params}. The captions of the subfigures indicate the amplitude voltage of the oscillator driving the EOM.}
  \label{fig_volt_plot}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width= 0.7 \textwidth]{img/bessel_singles.pdf}
	\caption{The values of the height plotted versus the respective modulation voltage. The first function $y = a_\mathrm{C} J_0^2(b_\mathrm{C} V)$ was fitted to the height of the carrier signal (red), where the fit results were $a_\mathrm{C} = \num{0.2 \pm 0.06}$ and $b_\mathrm{C} =  \SI{0.063 \pm 0.002}{\per \volt}$. In the fit of the height of the sideband (blue), one has to just use a first order Bessel function in the formula, the rest remains unchanged. The result is $a_\mathrm{S} = \num{0.2 \pm 0.005}$ with $b_\mathrm{S} = \SI{0.05 \pm 0.02}{\per \volt}$. The errors on the plot are mostly too small to be seen.}
	\label{fig_heights}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width= 0.7 \textwidth]{img/bessel_ratios.pdf}
	\caption{Ratio of the heights of the peaks of the carrier to the sideband. The fit function follows $y = J_0^2(b V) / J_1^2(b V)$, where we get $b = \SI{0.056(1)}{\per \volt}$. The errors on the data have been calculated with error propagation starting from the data in fig \ref{fig_heights}.}
	\label{fig_ratios}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width = 0.7 \textwidth]{img/fsr.pdf}
	\caption{Depiction of the FSR in the time domain. The centers of the fitted Lorentzian peaks are at \SI{0.6202 \pm 0.0002}{\ms} and \SI{9.2460 \pm 0.0002}{\ms}, resulting in a distance of \SI{8.6258 \pm 0.0003}{\ms}. }
	\label{fig_fsr}
\end{figure}

\subsection{Free spectral range}
We also measured the transmitted beam of the cavity to get a value for the free spectral range. The calculation is fairly straightforward. Firstly, one knows that the distance between the sidebands and the carrier is the modulation frequency \SI{13.55}{\mega \hertz} in the frequency domain. From the center positions of the sideband fits in fig. \ref{fig_volt_plot} one can further derive the distance in time domain. However, as was said earlier, that distance is not constant, but varies.
\\
We calculated the weighted mean of the distances between carrier and sideband respective center and obtained $\Delta t = \SI{0.1247 \pm 0.0002}{\milli \s}$, which corresponds to a conversion factor from time to frequency of \SI{108.7 \pm 0.1}{\mega \hertz \per \milli \s}.
\\
Now one can take a look at the measured free spectral range displayed in fig. \ref{fig_fsr}. Two Lorentzian functions were fitted into the peaks of the spectrum in order to extract distance in the time domain. Using the previously calculated conversion factor, we reach $\Delta \nu_\mathrm{fsr} = \SI{937(1)}{\mega \hertz}$. Using the formula
\begin{equation*}
	\Delta \nu_\mathrm{fsr} = \frac{c}{2 L},
\end{equation*}
where $L$ is the length of the Fabry-Perot etalon and $c$ is the speed of light, we can estimate that the length of the cavity was approximately \SI{16.0(2)}{\centi \meter}, which seems to be a plausible length.

\subsection{Pound Drever Hall signal}
In the last part of the analysis we took a look at the proper Pound Drever Hall signal. The modulation frequency was fixed at \SI{13.55}{\MHz}, and the amplitude at \SI{20}{\V} peak to peak. In each measurement the phase difference between the signal of the EOM and the signal that goes into the mixer was changed by 20 degrees. In our analysis first we converted the data from time to frequency, using the value obtained in the previous section. Then we applied the same filter of the previous section and fitted the data. The function fitted is based on equation \eqref{pdhsignal}
\[f(\omega) = V_\mathrm{off} + A\left[\text{Re}(F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega))\sin(\phi) + \text{Im}(F(\omega)F^*(\omega+ \Omega)-F^*(\omega)F(\omega-\Omega))\cos(\phi)\right],\]
where $V_\mathrm{off}$, $A$ are fit parameters. In the definition of $F(\omega)$ we included a parameter $\omega_0$ to account for a shift in the horizontal scale. We also used as parameters the reflectivity $r$ and the sideband frequency $\Omega$, even if this last value was know, but keeping it constant lead to problems during the fit. The results of the fit are shown in figures \ref{pdh1}-\ref{pdh7}. In every figure, the raw data are represented as blue lines, the orange lines are the filtered data, and the red lines are the fit. Several observations can be done on these results, first of all the phase angles. In summary, we obtained the following phases: $\SI{323.5\pm 0.4}{\degree},  \SI{303.5\pm 0.9}{\degree}, \SI{294.2\pm0.9}{\degree}, \SI{272.8\pm0.6}{\degree}, \SI{255.0\pm0.3}{\degree}, \SI{232.7\pm0.3}{\degree}, \SI{223.3\pm0.3}{\degree}$. A step of $\approx 20$ degrees is always present, apart in two cases: in fig \ref{pdh3}, where the step is $\approx 10$ degrees from the previous one, and in fig \ref{pdh7} where the step is again $\approx 10$ degrees.\\
The reflectivity of the mirrors is not always consistent, but we obtained values ranging from $0.99499$ to $0.99629$, which is a difference of approximately 0.1\%. However more problematic is the sideband frequency $\Omega$, which we expected to be \SI{13.55}{\MHz} within the error, but that is not always the case. From our fits we obtained a value of $\Omega$ ranging from \num{12.75} to \SI{14.69}{\MHz}. This behaviour is similar to what we have already seen in the previous part of the analysis. We see again an error on the $\Omega$, with a consistent order of magnitude in the error.

\begin{figure}[H]
  \centering
    \includegraphics[width =0.9 \textwidth]{img/pdh1}
    \caption{$ V_\mathrm{off}= \SI{-0.00112\pm 0.00001}{\V}$, $ A= \SI{0.0230 \pm 0.0001}{\V}$, $ \phi= \SI{-36.4\pm 0.4}{\degree}$, $ \omega_0= \SI{7280\pm 7}{\kHz}$, $ r= \SI{0.99629 \pm 0.00003}{}$, $ \Omega= \SI{13.24 \pm 0.03}{\MHz}$.}\label{pdh1}
\end{figure}
\begin{figure}[H]
  \centering
    \includegraphics[width =0.9 \textwidth]{img/pdh2}
    \caption{$ V_\mathrm{off}= \SI{-0.00084 \pm 0.00001}{\V} $, $ A= \SI{0.0284 \pm 0.0001}{\V}$, $ \phi= \SI{-56.4\pm 0.9}{\degree} $, $ \omega_0= \SI{7172\pm 8}{\kHz}$, $r= \SI{0.99527\pm 0.00004}{}$, $ \Omega= \SI{13.48 \pm 0.05}{\MHz}$. }\label{pdh2}
\end{figure}
\begin{figure}[H]
  \centering
    \includegraphics[width =0.9 \textwidth]{img/pdh3}
    \caption{$ V_\mathrm{off}= \SI{-0.00067 \pm 0.00001}{\V} $, $ A= \SI{0.0296\pm 0.0001}{\V} $, $ \phi= \SI{-65.7\pm 0.9}{\degree}$, $ \omega_0= \SI{5816\pm 8}{\kHz}$, $ r= \SI{0.99532\pm 0.00004}{}$, $ \Omega= \SI{12.75\pm 0.06}{\MHz}$. }\label{pdh3}
\end{figure}
\begin{figure}[H]
  \centering
    \includegraphics[width =0.9 \textwidth]{img/pdh4}
    \caption{$ V_\mathrm{off}= \SI{-0.00062\pm 0.00001}{\V}$, $A= \SI{0.0311\pm 0.0002}{\V}$, $ \phi= \SI{-87.2 \pm 0.6}{\degree}$, $ \omega_0= \SI{6668\pm 12}{\kHz}$, $r= \SI{0.99462\pm 0.00003}{}$, $ \Omega= \SI{14.69\pm 0.08}{\MHz}$. }\label{pdh4}
\end{figure}
\begin{figure}[H]
  \centering
    \includegraphics[width =0.9 \textwidth]{img/pdh5}
    \caption{$ V_\mathrm{off}= \SI{-0.00023 \pm 0.00001}{\V}$, $ A= \SI{0.0313\pm 0.0002}{\V}$, $ \phi= \SI{-104.9\pm 0.3}{\degree}$, $ \omega_0= \SI{-4349 \pm 13}{\kHz}$, $r= \SI{0.99562\pm 0.00002}{}$, $ \Omega= \SI{13.04\pm 0.05}{\MHz}$.}\label{pdh5}
\end{figure}
\begin{figure}[H]
  \centering
    \includegraphics[width =0.9 \textwidth]{img/pdh6}
    \caption{$ V_\mathrm{off}= \SI{-0.00047 \pm 0.00001}{\V}$, $A= \SI{0.0365\pm0.0003}{\V}$, $\phi= \SI{232.7\pm 0.3}{\degree}$, $\omega_0= \SI{6576\pm 15}{\kHz}$, $ r= \SI{0.99499\pm 0.00002}{}$, $\Omega= \SI{13.24 \pm 0.06}{MHz}$.}\label{pdh6}
\end{figure}
\begin{figure}[H]
  \centering
    \includegraphics[width =0.9 \textwidth]{img/pdh7}
    \caption{$ V_\mathrm{off}= \SI{-0.0004\pm 0.00002}{\V}$, $A= \SI{0.0290 \pm 0.0002}{\V}$, $ \phi= \SI{223.3\pm 0.3}{\degree}$, $ \omega_0= \SI{5699 \pm 16}{\kHz}$, $ r= \SI{0.99595\pm 0.00002}{}$, $ \Omega= \SI{13.10\pm 0.05}{\MHz}$.}\label{pdh7}
\end{figure}

\section{Conclusion}
The purpose of the experiment was to get an insight on the Pound-Drever-Hall technique. In this experiment, we first characterized the resonance of the EOM, which we found to be at \SI{13.65}{\mega \hertz}. Then we took a look at the signal after the EOM, so that we could see the carrier and the sidebands. We performed fits to the signals and compared the trend of the height ratios between the carrier and sidebands, with the ratios of the corresponding Bessel functions. We found a nice correlation, apart from the data point at \SI{10}{\volt}, which was completely off, probably due to a problem in the fit. In this part we also observed a relatively large variation in the modulation frequency compared to the setting on the function generator. We are not sure how to explain this problem, but most likely the setup was not ideal, the EOM was homemade, and the cavity was not in vacuum. In the last part of the experiment we measured the Pound-Drever-Hall signal which we fitted with the corresponding theoretical function. From this we tried to reproduce the phase step set in the instrument in the lab. We mostly found the same step size, apart from two cases, where the step given from the fit was only half of the expected one. This might be due to similar reasons as the variations observed on the modulation frequency.

\begin{thebibliography}{99}
\bibitem{script}
\textsc{Eric D. Black}, \textit{An introduction to Pound–Drever–Hall laser frequency stabilization}, 4 April 2000
\bibitem{saleh}
  \textsc{Bahaa E. A. Saleh, Malvin Carl Teich}, \textit{Fundamentals of photonics}, 2007, Wiley Series in Pure and Applied Optics, 2ed.
\end{thebibliography}

\end{document}
